from enum import Enum

class NEAT_CONFIG():
    def __init__(self):

        self.INPUTS: int = 2
        self.OUTPUTS: int = 1
        self.HIDDEN_NODES: int = 1000000
        self.POPULATION: int = 300
        
        self.COMPABILITY_THRESHOLD: int = 1
        self.EXCESS_COEFFICIENT: int = 2
        self.DISJOINT_COEFFICIENT: int = 2
        self.WEIGHT_COEFFICIENT: int = 0.4

        self.STALE_SPECIES: int = 15

        self.STEPS: int = 0.1
        self.PERTURB_CHANCE: int = 0.9
        self.WEIGHT_CHANGE: int = 0.3
        self.WEIGHT_MUTATION_CHANCE: int = 0.9
        self.NODE_MUTATION_CHANCE: int = 0.03
        self.CONNECTION_MUTATION_CHANCE: int = 0.05
        self.BIAS_CONNECTION_MUTATION_CHANCE: int = 0.15
        self.DISABLE_MUTATION_CHANCE: int = 0.1
        self.ENABLE_MUTATION_CHANCE: int = 0.2
        self.CROSSOVER_CHANCE: int = 0.75

        self.STALE_POOL: int = 20