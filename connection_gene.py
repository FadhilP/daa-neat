import neat_config

class ConnectionGene:
    def __init__(
        self, 
        into: int = 0, 
        out: int = 0, 
        innovation: int = 0, 
        weight: int = 0, 
        enabled: bool = 0, 
        connection_gene: 'ConnectionGene' = None
    ):
        
        self.into: int = into
        self.out: int = out
        self.innovation: int = innovation
        self.weight:int = weight
        self.enabled: bool = enabled

        if connection_gene:
            self.into = connection_gene.into
            self.out = connection_gene.out
            self.innovation = connection_gene.innovation
            self.weight = connection_gene.weight
            self.enabled = connection_gene.enabled

    def __str__(self):
        return f'{self.into}, {self.out}, {self.weight}, {self.enabled}, {self.innovation}'
