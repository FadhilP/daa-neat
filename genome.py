from collections import OrderedDict
from innovation_counter import InnovationCounter
import math
from connection_gene import ConnectionGene
from neat_config import NEAT_CONFIG
from node_gene import NodeGene
from random import *
from functools import total_ordering
from decimal import Decimal

config = NEAT_CONFIG()

@total_ordering
class Genome:
    def __init__(self, child: 'Genome' = None):
        self.rand: Random = Random()
        self.fitness: Decimal = 0
        self.points: Decimal = 0
        self.connection_gene_list: list[ConnectionGene] = []
        self.nodes: OrderedDict[int, NodeGene] = OrderedDict()
        self.adjusted_fitness: float = 0

        if not child:
            self.mutation_rates: dict[str, float] = {
                'STEPS': config.STEPS,
                'PERTURB_CHANCE': config.PERTURB_CHANCE,
                'WEIGHT_CHANCE': config.WEIGHT_CHANGE,
                'WEIGHT_MUTATION_CHANCE': config.WEIGHT_MUTATION_CHANCE,
                'NODE_MUTATION_CHANCE': config.NODE_MUTATION_CHANCE,
                'CONNECTION_MUTATION_CHANCE': config.CONNECTION_MUTATION_CHANCE,
                'BIAS_CONNECTION_MUTATION_CHANCE': config.BIAS_CONNECTION_MUTATION_CHANCE,
                'DISABLE_MUTATION_CHANCE': config.DISABLE_MUTATION_CHANCE,
                'ENABLE_MUTATION_CHANCE': config.ENABLE_MUTATION_CHANCE
            }

        else:
            connection_gene: ConnectionGene
            for connection_gene in child.connection_gene_list:
                self.connection_gene_list.append(ConnectionGene(connection_gene=connection_gene))

            self.fitness = child.fitness
            self.adjusted_fitness = child.adjusted_fitness

            self.mutation_rates = child.mutation_rates.copy()

        
    @staticmethod
    def cross_over(parent1: 'Genome', parent2: 'Genome') -> 'Genome':
        if parent1.fitness < parent2.fitness:
            parent1, parent2 = parent2, parent1

        child: Genome = Genome()
        gene_map1: OrderedDict[int, ConnectionGene] = OrderedDict()
        gene_map2: OrderedDict[int, ConnectionGene] = OrderedDict()

        con: ConnectionGene
        for con in parent1.connection_gene_list:
            if con.innovation not in gene_map1:
                gene_map1[con.innovation] = con

        for con in parent2.connection_gene_list:
            if con.innovation not in gene_map2:
                gene_map2[con.innovation] = con

        # ! NEED TEST ! #
        innovation_p1 = gene_map1.keys()
        innovation_p2 = gene_map2.keys()

        all_innovations: set[int] = list(set(innovation_p1) | set(innovation_p2))

        for key in all_innovations:
            trait: ConnectionGene = None

            if key in gene_map1 and key in gene_map2:
                if bool(getrandbits(1)):
                    trait = ConnectionGene(connection_gene=gene_map1[key])
                else:
                    trait = ConnectionGene(connection_gene=gene_map2[key])

                if gene_map1[key].enabled == gene_map2[key].enabled:
                    if random() < 0.75:
                        trait.enabled = False
                    else:
                        trait.enabled = True

            elif parent1.fitness == parent2.fitness:
                if key in gene_map1:
                    trait = gene_map1[key]
                else:
                    trait = gene_map2[key]

                if bool(getrandbits(1)):
                    continue

            else:
                if key in gene_map1:
                    trait = gene_map1[key]

                else:
                    continue

            child.connection_gene_list.append(trait)

        return child

        

    @staticmethod
    def is_same_species(g1: 'Genome', g2: 'Genome') -> bool:
        gene_map1: OrderedDict[int, ConnectionGene] = OrderedDict()
        gene_map2: OrderedDict[int, ConnectionGene] = OrderedDict()

        matching: int = 0
        disjoint: int = 0
        excess: int = 0
        weight: float = 0
        low_max_innovation: int = None
        delta: float = 0

        con: ConnectionGene
        for con in g1.connection_gene_list:
            gene_map1[con.innovation] = con

        for con in g2.connection_gene_list:
            gene_map2[con.innovation] = con

        if not len(gene_map1) or not len(gene_map2):
            low_max_innovation = 0
        else:
            low_max_innovation = min(list(gene_map1.keys())[-1], list(gene_map2.keys())[-1])

        # ! NEED TEST ! #
        all_innovations: set[int] = set([*gene_map1.keys(), *gene_map2.keys()])
       
        for key in all_innovations:
            if key in gene_map1 and key in gene_map2:
                matching += 1
                weight += abs(gene_map1[key].weight - gene_map2[key].weight)

            else:
                if key < low_max_innovation:
                    disjoint += 1
                else:
                    excess += 1

        N: int = matching + disjoint + excess
       
        if N > 0:
            matching += 1
            delta = (config.EXCESS_COEFFICIENT * excess + config.DISJOINT_COEFFICIENT * disjoint) / N + (config.WEIGHT_COEFFICIENT * weight) / matching

        return delta < config.COMPABILITY_THRESHOLD

        
    def generate_network(self):
        self.nodes.clear()
        for i in range(config.INPUTS):
            self.nodes[i] = NodeGene(0)

        self.nodes[config.INPUTS] = NodeGene(1)

        for i in range(config.INPUTS + config.HIDDEN_NODES, config.INPUTS + config.HIDDEN_NODES + config.OUTPUTS):
            self.nodes[i] = NodeGene(0)

        con: ConnectionGene
        for con in self.connection_gene_list:
            if not con.into in self.nodes:
                self.nodes[con.into] = NodeGene(0)
            if not con.out in self.nodes:
                self.nodes[con.out] = NodeGene(0)
            self.nodes.get(con.out).incoming_con.append(con)
    
    def evaluate_network(self, inputs: list[float]) -> list[float]:
        output: list[float] = [None] * config.OUTPUTS
        self.generate_network()

        for i in range(config.INPUTS):
            self.nodes.get(i).value = inputs[i]

        i: int
        node_gene: NodeGene
        for i, node_gene in self.nodes.items():
            sum: float = 0
            key: int = i
            node: NodeGene = node_gene
           
            if key > config.INPUTS:
                conn: ConnectionGene
                for conn in node.incoming_con:
                    if conn.enabled:
                        sum += self.nodes.get(conn.into).value * conn.weight
                        
                # * Sigmoid Function
                node.value = 1 / (1 + math.exp(-sum))

        for i in range(config.OUTPUTS):
            output[i] = self.nodes.get(config.INPUTS + config.HIDDEN_NODES + i).value
           
        return output

    def mutate(self):
        for key, value in self.mutation_rates.items():
            if bool(randbytes(1)):
                self.mutation_rates[key] = 0.95 * value
            else:
                self.mutation_rates[key] = 1.05263 * value

        if random() <= self.mutation_rates['WEIGHT_MUTATION_CHANCE']: self.mutate_weight()
        if random() <= self.mutation_rates['CONNECTION_MUTATION_CHANCE']: self.mutate_add_connection(False)
        if random() <= self.mutation_rates['BIAS_CONNECTION_MUTATION_CHANCE']: self.mutate_add_connection(True)
        if random() <= self.mutation_rates['NODE_MUTATION_CHANCE']: self.mutate_add_node()
        if random() <= self.mutation_rates['DISABLE_MUTATION_CHANCE']: self.disable_mutate()
        if random() <= self.mutation_rates['ENABLE_MUTATION_CHANCE']: self.enable_mutate()


    def mutate_weight(self):
        c: ConnectionGene
        for c in self.connection_gene_list:
            if random() < config.WEIGHT_CHANGE:
                if random() < config.PERTURB_CHANCE:
                    c.weight = c.weight + (2 * random() - 1) * config.STEPS
                else:
                    c.weight = 4 * random() - 2

    def mutate_add_connection(self, forceBais: bool):
        self.generate_network()
        i: int = 0
        j: int = 0
        random1: int = randint(0, len(self.nodes) - config.INPUTS - 1) + config.INPUTS + 1
        random2: int = randint(0, len(self.nodes))

        if forceBais:
            random1 = config.INPUTS
        node1: int = -1
        node2: int = -1

        k: int
        for k in self.nodes.keys():
            if random1 == i:
                node1 = k
                break
            i += 1
    
        for k in self.nodes.keys():
            if random2 == j:
                node2 = k
                break
            j += 1

        if node1 >= node2:
            return

        con: ConnectionGene
        for con in self.nodes.get(node2).incoming_con:
            if con.into == node1:
                return

        if node1 < 0 or node2 < 0:
            return

        self.connection_gene_list.append(ConnectionGene(node1, node2, InnovationCounter.new_innovation(), 4 * random() - 2, True))


    def mutate_add_node(self):
        self.generate_network()
        if len(self.connection_gene_list) > 0:
            timeout_count: int = 0
            random_con: ConnectionGene = choice(self.connection_gene_list)
            while not random_con.enabled:
                random_con = choice(self.connection_gene_list)
                timeout_count += 1
                if timeout_count > config.HIDDEN_NODES:
                    return
            
            next_node: int = len(self.nodes) - config.OUTPUTS
            random_con.enabled = False
            self.connection_gene_list.append(ConnectionGene(random_con.into, next_node, InnovationCounter.new_innovation(), 1, True))
            self.connection_gene_list.append(ConnectionGene(next_node, random_con.out, InnovationCounter.new_innovation(), random_con.weight, True))


    def disable_mutate(self):
        if len(self.connection_gene_list) > 0:
            random_con: ConnectionGene = choice(self.connection_gene_list)
            random_con.enabled = False

    def enable_mutate(self):
        if len(self.connection_gene_list) > 0:
            random_con: ConnectionGene = choice(self.connection_gene_list)
            random_con.enabled = True

    def __eq__(self, other: 'Genome'):
        return self.fitness == other.fitness

    def __ne__(self, other: 'Genome'):
        return self.fitness != other.fitness

    def __lt__(self, other: 'Genome'):
        return self.fitness < other.fitness

    def __str__(self):
        return f'''Genome{{
            fitness = {self.fitness}
            connectionGeneList = {self.connection_gene_list}
            nodeGenes = {self.nodes}
            }}'''

    def __repr__(self):
         return f'''Genome{{
            fitness = {self.fitness}
            connectionGeneList = {self.connection_gene_list}
            nodeGenes = {self.nodes}
            }}'''

    def write(self, method):
        file = open("genome.txt", method)
        builder: str = ""

        conn: ConnectionGene
        for conn in self.connection_gene_list:
            builder += f'{str(conn)} \n'

        file.write(builder)
        file.close()

