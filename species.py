import math
from neat_config import NEAT_CONFIG 
import random
from genome import Genome
from functools import total_ordering

config = NEAT_CONFIG()

@total_ordering
class Species:
    def __init__(self, top: Genome = None):
        self.genomes: list[Genome] = []
        self.top_fitness: float = 0
        self.staleness: int = 0
        self.rand = random.Random()

        if top:
            self.genomes.append(top)

    def calculate_genome_adjusted_fitness(self):
        g: Genome
        for g in self.genomes:
            g.adjusted_fitness = g.fitness / len(self.genomes)

    def get_total_adjusted_fitness(self) -> float:
        total_adjusted_fitness: float = 0
        g: Genome
        for g in self.genomes:
            total_adjusted_fitness += g.adjusted_fitness

        return total_adjusted_fitness

    def sort_genomes(self):
        self.genomes = list(reversed(sorted(self.genomes)))

    def remove_weak_genomes(self, all_but_one: bool):
        self.sort_genomes()
        survive_count: int = 1
        if not all_but_one:
            survive_count = math.ceil(len(self.genomes)/2)
        
        survived_genomes: list[Genome] = []
        for i in range(survive_count):
            survived_genomes.append(Genome(self.genomes[i]))

        self.genomes = survived_genomes

    def get_top_genome(self) -> Genome:
        self.sort_genomes()
        return self.genomes[0];

    def breed_child(self) -> Genome:
        child: Genome
        if self.rand.random() < config.CROSSOVER_CHANCE:
            g1: Genome = self.rand.choice(self.genomes)
            g2: Genome = self.rand.choice(self.genomes)
            child = Genome.cross_over(g1, g2)

        else:
            g1: Genome = self.rand.choice(self.genomes)
            child = g1
        
        child = Genome(child)
        child.mutate()

        return child

    def __eq__(self, other: 'Species'):
        return self.top_fitness == other.top_fitness

    def __ne__(self, other: 'Species'):
        return self.top_fitness != other.top_fitness

    def __lt__(self, other: 'Species'):
        return self.top_fitness < other.top_fitness

