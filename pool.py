from neat_config import NEAT_CONFIG
from environment import Environment
from species import Species
from neat_config import NEAT_CONFIG
from genome import Genome

config = NEAT_CONFIG()

class Pool:
    
    def __init__(self):
        self.species: list[Species] = []
        self.generations: int = 0
        self.top_fitness: float = 0
        self.pool_staleness: int = 0

    def initialize_pool(self):
        for _ in range(config.POPULATION):
            self.add_to_species(Genome())

    def add_to_species(self, g: Genome): 
        s: Species
        for s in self.species:
            if len(s.genomes) == 0:
                continue
            
            g0: Genome = s.genomes[0]
            if Genome.is_same_species(g, g0):
                s.genomes.append(g)
                return

        child_species: Species = Species()
        child_species.genomes.append(g)
        self.species.append(child_species)

    # def evaluate_fitness(self):
        # s: Species
        # for s in self.species:
        #     g: Genome
        #     for g in s.genomes:
    #             fitness: float = 0
    #             g.fitness = 0
    #             for i in range(2):
    #                 for j in range(2):
    #                     inputs: list[float] = [i, j]
    #                     output: list[float] = g.evaluate_network(inputs)
    #                     expected: int = i**j
    #                     fitness += (1 - abs(expected - output[0]))
                    
    #             fitness *= fitness

    #         ## TODO: CONTINUE

    def evaluate_fitness(self, environment: Environment):
        all_genome: list[Genome] = []

        s: Species
        for s in self.species:
            g: Genome
            for g in s.genomes:
                all_genome.append(g)

        environment.evaluate_fitness(all_genome)
        self.rank_globally()

    def rank_globally(self):
        all_genome: list[Genome] = []

        s: Species
        for s in self.species:
            g: Genome
            for g in s.genomes:
                all_genome.append(g)

        all_genome = list(reversed(sorted(all_genome)))

        for i in range(len(all_genome)):
            all_genome[i].points = all_genome[i].fitness
            all_genome[i].fitness = i

    def get_top_genome(self) -> Genome:
        all_genome: list[Genome] = []
        s: Species
        for s in self.species:
            g: Genome
            for g in s.genomes:
                all_genome.append(g)

        all_genome = list(sorted(all_genome))
        
        return all_genome[-1]

    def calculate_global_adjusted_fitness(self) -> float:
        total: float = 0
        s: Species
        for s in self.species:
            total += s.get_total_adjusted_fitness()

        return total

    def remove_weak_genome_from_species(self, all_but_one: bool):
        s: Species
        for s in self.species:
            s.remove_weak_genomes(all_but_one)

    def remove_stale_species(self):
        survived: list[Species] = []

        if self.top_fitness < self.get_top_fitness():
            self.pool_staleness = 0

        s: Species
        for s in self.species:
            top: Genome = s.get_top_genome()
            if top.fitness > s.top_fitness:
                s.top_fitness = top.fitness
                s.staleness = 0

            else:
                s.staleness += 1

            if s.staleness < config.STALE_SPECIES or s.top_fitness >= self.top_fitness:
                survived.append(s)

        survived = list(reversed(sorted(survived)))

        if self.pool_staleness > config.STALE_POOL:
            for i in range(len(survived), 1, -1):
                survived.pop(i)

        self.species = survived
        self.pool_staleness += 1

    def calculate_genome_adjusted_fitness(self):
        s: Species
        for s in self.species:
            s.calculate_genome_adjusted_fitness()

    def breed_new_generation(self) -> list[Genome]:
        self.calculate_genome_adjusted_fitness()
        survived: list[Genome] = []

        self.remove_weak_genome_from_species(False)
        self.remove_stale_species()

        global_adjusted_fitness: float = self.calculate_global_adjusted_fitness()
        children: list[Genome] = []
        carry_over: float = 0
        
        for s in self.species:
            fchild: float = config.POPULATION * (s.get_total_adjusted_fitness() / global_adjusted_fitness)
            nchild: int = fchild
            carry_over += fchild - nchild
            if carry_over > 1:
                nchild += 1
                carry_over -= 1

            if nchild < 1:
                continue

            survived.append(Species(s.get_top_genome()))

            for _ in range(1, int(nchild)):
                child: Genome = s.breed_child()
                children.append(child)

        self.species = survived

        for child in children:
            self.add_to_species(child)

        self.generations += 1
        return children

    def get_top_fitness(self) -> float:
        top_fitness: float = 0
        top_genome: Genome = None
        s: Species
        for s in self.species:
            top_genome = s.get_top_genome()
            if top_genome.fitness > top_fitness:
                top_fitness = top_genome.fitness


        return top_fitness

    def get_current_population(self) -> int:
        p: int = 0
        s: Species
        for s in self.species:
            p += len(s.genomes)
        return p
