from innovation_counter import InnovationCounter
from genome import Genome
from environment import Environment
from pool import Pool
from decimal import Decimal

class XOR(Environment):
    def evaluate_fitness(self, population: list[Genome]):
        gene: Genome
        for gene in population:
            fitness: Decimal = 0
            gene.fitness = 0
            gene.write('w')
            for i in range(2):
                for j in range(2):
                    inputs: list[float] = [i, j]
                    output: list[float] = gene.evaluate_network(inputs)
                    expected: int = i^j

                    fitness += Decimal((1 - abs(expected - output[0])))
            
            fitness = fitness ** 2

            gene.fitness = fitness
            # gene.write('a')

def main():
    xor: XOR = XOR()

    pool: Pool = Pool()
    pool.initialize_pool()

    top_genome: Genome = Genome()
    generation: int = 0

    while(True):
        pool.evaluate_fitness(xor)
        top_genome = pool.get_top_genome()
        print(f'Top Fitness: {top_genome.points}')

        if top_genome.points > 15:
            break

        print(f'Generation: {generation}')

        pool.breed_new_generation()
        generation += 1

main()

