from connection_gene import ConnectionGene

class NodeGene:
    def __init__(self, value):
        self.value: float = value
        self.incoming_con: list[ConnectionGene] = []