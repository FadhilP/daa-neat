class InnovationCounter:
    innovation = 0
    @classmethod
    def new_innovation(cls):
        InnovationCounter.innovation += 1
        return InnovationCounter.innovation